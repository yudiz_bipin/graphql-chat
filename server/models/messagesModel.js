const mongoose = require('mongoose')

const Messages = mongoose.Schema({
    message: {type:String, required:true},
    senderMail: {type:String, required:true},
    receiverMail: {type:String, required:true},
    iUserId:{type:mongoose.SchemaTypes.ObjectId}
    
},{timestamps:{createdAt:"dCreatedAt",updatedAt:"dUpdatedAt"}})

module.exports = mongoose.model('messages',Messages)