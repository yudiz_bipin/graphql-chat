module.exports = `

enum CacheControlScope {
    PUBLIC
    PRIVATE
  }
  
  directive @cacheControl(
    maxAge: Int
    scope: CacheControlScope
    inheritMaxAge: Boolean
  ) on FIELD_DEFINITION | OBJECT | INTERFACE | UNION
  
    type Query {
        users: [User] @cacheControl(maxAge:30)
        messages(id:String): [Message] @cacheControl(maxAge:30)
    }

    type User {
        id: ID!
        name: String!
        email: String!
    }

    type Message {
        id: ID!
        message: String!
        senderMail: String!
        receiverMail: String!
        iUserId:ID
    }

    type Mutation {
        
        createUser(name:String!,email:String!): User!
        updateUser(id:ID!, name:String): User!
        deleteUser(email:String!): String!
        userTyping(email:String receiverMail:String!):Boolean!

        createMessage(senderMail:String!, receiverMail:String!, message:String!): Message!
        updateMessage(id:ID!, message:String!): Message!
        deleteMessage(id:String!): Boolean!

    }

    type Subscription {
        newMessage(receiverMail:String!): Message
        userTyping(receiverMail:String!): String
        newUser : User
        oldUser : String
    }

`;
