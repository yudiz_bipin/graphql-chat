const {ApolloServer} = require('apollo-server-express')
const app = require('express')()
const mongoose = require('mongoose')
const {createServer} = require('http')
const {execute,subscribe} = require('graphql')
const {SubscriptionServer} = require('subscriptions-transport-ws')
const {makeExecutableSchema} = require('@graphql-tools/schema')
const {ApolloServerPluginLandingPageGraphQLPlayground} = require('apollo-server-core')
const {PubSub}= require('graphql-subscriptions')
const {ApolloServerPluginCacheControl} = require('apollo-server-core')


const pubsub = new PubSub()

const typeDefs = require('./typeDefs')
const resolvers = require('./resolvers')


//db
mongoose.connect('mongodb+srv://bipin2:qum2U5JQhFnTP8yQ@cluster0.a2mgt.mongodb.net/graphql-chat')
.then(()=>console.log('database connected...'))
.catch(error=>console.log('error in db connection'))

const schema = makeExecutableSchema({typeDefs,resolvers})


async function startServer() {
    
    const httpServer = createServer(app)
    const server = new ApolloServer({
        schema,
        plugins:[
            {
            async serverWillStart(){
                return{
                    async drainServer(){
                        subscriptionServer.close()
                    }
                }
            }
        },
        ApolloServerPluginLandingPageGraphQLPlayground(), 
        ApolloServerPluginCacheControl({defaultMaxAge:5})
    ]
    })
    
    await server.start()

    server.applyMiddleware({app})
    
    const subscriptionServer = SubscriptionServer.create({
        schema,
        execute,
        subscribe,
    },{
        server:httpServer,
        path:server.graphqlPath
    })
    
    const PORT = process.env.PORT || 4000

    httpServer.listen(PORT,()=>{
        console.log(`server is now running on http://localhost:${PORT}/graphql`)
    })
}

startServer()

